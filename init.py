from database import init_db, db_session

if __name__ == '__main__':
    init_db()
    db_session.commit()
