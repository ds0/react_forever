from flask_wtf import FlaskForm
from flask.ext.login import UserMixin
from wtforms import StringField, HiddenField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length
from flask_security.forms import RegisterForm, LoginForm
from models.user import User
import socket
from urllib.parse import urlparse
import requests
import re

class ExtendedRegisterForm(RegisterForm):
    username = StringField('Username', [DataRequired(), Length(min=1,max=16)])

    def validate(self):
        validation = FlaskForm.validate(self)
        if not validation:
            return False       

        if not re.match("^[a-zA-Z0-9_-]+$", self.username.data):
            self.username.errors.append('You can only use a-z, A-Z, 0-9, _, and - characters. Sorry.')
            return False
    
        user_check = User.query.filter_by(username=self.username.data).first()
        if user_check is not None:
            self.username.errors.append('Username already in use')
            return False

        return True

class ReactionForm(FlaskForm):
    link = StringField('', [DataRequired()], render_kw={"placeholder": "Paste a link here."})
    emoji = HiddenField('', [DataRequired()])
    submit = SubmitField('Post it.')

    def validate(self):
        okay = True
        url = str(self.link.data)
        if not url.strip():
            self.link.errors += ('Hey, you gotta put a link here.',)
            okay = False
        else:
            do_status_check = True
            if not url.startswith('http://') and not url.startswith('https://'):
                url = 'http://'+url
            self.link.data = url
            parsed_url = urlparse(url)
            query_url = ""
            if parsed_url.netloc:
                query_url = parsed_url.netloc
            else:
                query_url = parsed_url.path
            try:
                s = socket.gethostbyname(query_url)
            except socket.gaierror:
                self.link.errors += ("Hey, that's not really a valid link.",)
                okay = False
                do_status_check = False
            if do_status_check and str(requests.get(url).status_code)[0] == '4':
                print(url)
                self.link.errors += ("Hey, that link just redirects to a 404 page.",)
                okay = False
        if not self.emoji.data:
            self.emoji.errors += ('Hey, you gotta pick an emoji.',)
            okay = False
        return okay


