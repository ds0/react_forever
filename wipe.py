import database
import sys

if __name__ == '__main__':
    database.init_db()
    wiped= False
    if 'post' in sys.argv:
        database.wipe_reactions()
        wiped = True
    if 'user' in sys.argv:
        database.wipe_users()
        wiped = True
    if wiped:
        print("wiped...")

