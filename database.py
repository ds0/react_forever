from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:////tmp/test.db', \
                       convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import models
    Base.metadata.create_all(bind=engine)

def wipe_reactions():
    from models import reaction
    reaction.Link.query.delete()
    reaction.Reaction.query.delete()
    db_session.commit()

def wipe_users():
    from models import user
    user.User.query.delete()
    user.Role.query.delete()
    user.RolesUsers.query.delete()
    db_session.commit()
