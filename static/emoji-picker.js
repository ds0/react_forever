var emoji_json = (function () {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/static/emoji-lookup.json',
		'dataType': "json",
		'success': function (data) {
			json = data;
		}
	});
	return json;
})();

$(document).ready(function () {
	var active_emoji = $("#emoji").val();
	if(active_emoji != "") {
		evaluate_active_emoji(active_emoji);
	}
	var active_reaction = "";
	var active_group = "smileys-and-people";
	evaluate_active_group(active_group);
	var active_tone = "a";
	var visible_form = false;
	var stickyHeaderTop = $('#reaction-menu').offset().top;
	draw_reaction_emoji();

	function evaluate_active_emoji(emoji_id) {
		var selection = "#"+active_emoji;
		if(selection != "#") {
			$(selection).css("background-color", "#fafaff");
			$(selection+" > .emoji").css("border-top", "8px solid #fafaff");
		}
		active_emoji = emoji_id;
		selection = "#"+active_emoji;
		if(selection != "#") {
			$(selection).css("background-color", "#cde");
			$(selection+" > .emoji").css("border-top", "8px solid #cde");
		}
	}

	$(window).scroll(function(){
		if( $(window).scrollTop() > stickyHeaderTop ) {
			$('#reaction-menu').css({position: 'fixed', left: '0px', top: '0px', width: '100vw'});
		} else {
			$('#reaction-menu').css({position: 'static', top: '0px', width: '100%'});
		}
	});

	function fill_emoji_list(group) {
		var items = [];
		$.each(emoji_json[group], function(emoji, e_data) {
			var sheet = "a";
			if("tones" in e_data) {
				sheet = active_tone;
			}
			items.push("<div class='emoji-container' id='"+emoji+"' data-value='"+emoji+"'><div class='emoji' style='background-image: url(/static/img/sheet_"+sheet+".png); background-position: -"+e_data['x']+"px -"+e_data['y']+"px;'></div></div>");
		});
		$("#emoji-picker").html(items);
		set_skin_tone(active_tone);
	}
	fill_emoji_list("smileys-and-people");

	$(".group-container").click(function() {
		group = $(this).attr("id");
		fill_emoji_list(group);
		evaluate_active_group(group);
	});

	function evaluate_active_group(group_id) {
		var selection = "#"+active_group;
		if(selection != "#") {
			$(selection).css("background-color", "#fafaff");
			$(selection).css("border-color", "#fafaff");
		}
		active_group = group_id;
		selection = "#"+active_group;
		if(selection != "#") {
			$(selection).css("background-color", "#cde");
			$(selection).css("border-color", "#cde");
		}
	
	}	

	$(".tone-container").click(function() {
		set_skin_tone($(this).children(":first").attr("id").slice(-1));
	});

	function set_skin_tone(tone) {
		$.each(emoji_json["smileys-and-people"], function(emoji, e_data) {
			if("tones" in e_data) {
				var toned_code = emoji;
				var toned_x = e_data["x"];
				var toned_y = e_data["y"];
				if(tone != "a") {
					$.each(e_data["tones"], function(t, t_data) {
						if(tone == t[0]) {
							toned_code = t.split(" ")[1];
							toned_x = t_data["x"];
							toned_y = t_data["y"];
							return false;
						}
					});
				}
				$("#"+emoji).attr("data-value", toned_code);
				$("#"+emoji).html("<div class='emoji' style='background-image: url(/static/img/sheet_"+tone+".png); background-position: -"+toned_x+"px -"+toned_y+"px;'></div>");
			}
		});
		evaluate_active_tone(tone);
	}

	function evaluate_active_tone(tone) {
		var selection = "#tone-"+active_tone;
		if(selection != "#") {
			$(selection).css("border-color", "#fafaff");
		}
		active_tone = tone;
		selection = "#tone-"+active_tone;
		if(selection != "#") {
			$(selection).css("border-color", "#cde");
		}
		if(active_emoji != "") {
			set_emoji(active_emoji);
		}
	}

	function draw_reaction_emoji() {
		$.each($(".reaction"), function() {
			var emoji = $(this).attr("data-value");
			var tone = "a";
			var x = "";
			var y = "";
			$.each(emoji_json, function(group, g_data) {
				if(emoji in g_data) {
					x = g_data[emoji]['x'];
					y = g_data[emoji]['y'];
					return false;
				}
				if(group == 'smileys-and-people') {
					$.each(emoji_json['smileys-and-people'], function(e, e_data) {
						var found_emoji = false;
						if("tones" in e_data) {
							$.each(e_data["tones"], function(t,t_data) {
								if(emoji == t.split(" ")[1]) {
									tone = t.split(" ")[0];
									x = t_data['x'];
									y = t_data['y'];
									found_emoji = true;
									return false;
								}
							});
						}
						if (found_emoji == true) {
							return false;
						}
					});
				}
			});
			$(this).css("background-image", "url(/static/img/sheet_"+tone+".png)");
			$(this).css("background-position","-"+x+"px -"+y+"px");
		});
	}

	$(".reaction-container").click(function() {
		$("#reaction-menu").html($(this).find(".reaction-info").clone());
		var reaction_id = $(this).attr("id");
		evaluate_active_reaction(reaction_id);
	});

	function evaluate_active_reaction(reaction_id) {
		var selection = "#"+active_reaction;
		if(selection != "#") {
			$(selection).css("background-color", "#fafaff");
			$(selection+" > .reaction").css("border-top", "8px solid #fafaff");
		}
		active_reaction = reaction_id;
		selection = "#"+active_reaction;
		if(selection != "#") {
			$(selection).css("background-color", "#cde");
			$(selection+" > .reaction").css("border-top", "8px solid #cde");
		}
	}

	$("#start-form").click(function() {
		if (visible_form == false) {
			$(".form-link-container").next().css("display", "block");
			$(this).html("Or maybe not.");
			visible_form = true;
			stickyHeaderTop = $('#reaction-menu').offset().top;
		} else {
			$(".form-link-container").next().css("display", "none");
			$(this).html("Paste a link.");
			visible_form = false;
			stickyHeaderTop = $('#reaction-menu').offset().top;
		}
	});

	if ($(".error").length > 0) {
		$("#start-form").trigger('click');
	}

	function set_emoji(emoji_id) {
		var emoji = $("#"+emoji_id).attr("data-value");
		$("#emoji").val(emoji);
		evaluate_active_emoji(emoji_id);
	}

	$(document).on("click", "div.emoji-container", function() {
		set_emoji($(this).attr("id"));
	});

});
