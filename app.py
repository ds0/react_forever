from flask import Flask, render_template, render_template_string, session, \
    redirect, url_for, request
from flask_security import Security, login_required, \
     SQLAlchemySessionUserDatastore, current_user
from database import db_session, init_db
from models import user, reaction
from forms import *

# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-salt'
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_REGISTER_URL'] = '/register'

# Setup Flask-Security
user_datastore = SQLAlchemySessionUserDatastore(db_session,
                                                user.User, user.Role)
security = Security(app, user_datastore, register_form=ExtendedRegisterForm, login_form=LoginForm)

@app.before_first_request
def setup():
    init_db()

# Views
@app.route('/', methods=["GET", "POST"])
def home():
    posts = reaction.Reaction.query.all()[::-1]
    if request.method == 'GET':
        form=None
        if current_user.is_authenticated:
            form = ReactionForm()
        return render_template('home.html', form=form, posts=posts)
    elif request.method =='POST' and current_user.is_authenticated:
        form = ReactionForm()
        if form.validate_on_submit():
            l = reaction.Link.query.filter_by(url=form.link.data).first()
            if l is None:
                l = reaction.Link(url=form.link.data)
                db_session.add(l)
                db_session.commit()

            r = reaction.Reaction.query.filter_by(
                link_id=l.id,
                user_id=session['user_id']).first()
            if r is None:
                r = reaction.Reaction(
                        user_id=session['user_id'],
                        link_id=l.id,
                        emoji=form.emoji.data)
                db_session.add(r)
                db_session.commit()
            elif form.emoji.data != r.emoji:
                r.emoji = form.emoji.data
                db_session.commit()
            return redirect(url_for('home'))
        return render_template('home.html', form=form, posts=posts)

@app.route('/user/<uname>')
def show_user(uname):
    u = user.User.query.filter_by(username=uname).first()
    if u is None:
        return render_template('404.html')
    fwer_count = user.UserRelationship.query.filter_by(following_id=u.id).count()
    fwing_count = user.UserRelationship.query.filter_by(follower_id=u.id).count()
    
    following = None
    if current_user.is_authenticated and session['user_id'] != u.id:
        fw = user.UserRelationship.query.filter_by(following_id=u.id, follower_id=session['user_id']).first()
        following = True if fw else False

    follower = None
    if current_user.is_authenticated and session['user_id'] != u.id:
        fw = user.UserRelationship.query.filter_by(following_id=session['user_id'], follower_id=u.id).first()
        follower = True if fw else False

    posts = reaction.Reaction.query.filter_by(user_id=u.id)[::-1]
    return render_template('user.html', user=u, posts=posts, fwer_count=fwer_count, fwing_count=fwing_count, follower=follower, following=following)

@app.route('/user/<uname>/followers')
def show_followers(uname):
    u = user.User.query.filter_by(username=uname).first()
    if u is None:
        return render_template('404.html')
    fw_list = user.UserRelationship.query.filter_by(following_id=u.id)
    return render_template('follow_list.html', user=u, fw_list=fw_list, style="followers")

@app.route('/user/<uname>/following')
def show_following(uname):
    u = user.User.query.filter_by(username=uname).first()
    if u is None:
        return render_template('404.html')
    fw_list = user.UserRelationship.query.filter_by(follower_id=u.id)
    return render_template('follow_list.html', user=u, fw_list=fw_list, style="following")

@login_required
@app.route('/user/<uname>/follow', methods=['POST'])
def follow(uname):
    u = user.User.query.filter_by(username=uname).first()
    if u is None:
        return render_template('404.html')
    fw = user.UserRelationship.query.filter_by(following_id=u.id, follower_id=session['user_id']).first()
    if fw is None and u.id != session['user_id']:
        fw = user.UserRelationship(following_id=u.id, follower_id=session['user_id'])
        db_session.add(fw)
        db_session.commit()
    return redirect(url_for('show_user', uname=u.username))

@login_required
@app.route('/user/<uname>/unfollow', methods=['POST'])
def unfollow(uname):
    u = user.User.query.filter_by(username=uname).first()
    if u is None:
        return render_template('404.html')
    fw_query = user.UserRelationship.query.filter_by(following_id=u.id, follower_id=session['user_id'])
    fw = fw_query.first()
    if fw is not None and u.id != session['user_id']:
        fw_query.delete()
        db_session.commit()
    return redirect(url_for('show_user', uname=u.username))

if __name__ == '__main__':
    app.run()

