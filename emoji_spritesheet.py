import subprocess
import shutil
import requests
import json
from collections import OrderedDict

def init_spritesheet(name,x,y):
    cmd = "convert -size "+x+"x"+y+" xc:transparent png24:"+name
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

def download_emojis():
    d = OrderedDict()
    current_group = ""
    i = {'a':0, 'b':0, 'c':0, 'd':0, 'e':0, 'f':0}
    exceptions=["1f46e", "1f575", "1f482", "1f477", "1f9d9", "1f9da", "1f9db", "1f9dd", "1f9de", "1f9df", "1f48f", "1f491", "1f46a", "1f46e-1f3fb", "1f575-1f3fb", "1f482-1f3fb", "1f477-1f3fb", "1f9d9-1f3fb", "1f9da-1f3fb", "1f9db-1f3fb", "1f9dd-1f3fb", "1f9de-1f3fb", "1f9df-1f3fb", "1f48f-1f3fb", "1f491-1f3fb", "1f46a-1f3fb", "1f46e-1f3fc", "1f575-1f3fc", "1f482-1f3fc", "1f477-1f3fc", "1f9d9-1f3fc", "1f9da-1f3fc", "1f9db-1f3fc", "1f9dd-1f3fc", "1f9de-1f3fc", "1f9df-1f3fc", "1f48f-1f3fc", "1f491-1f3fc", "1f46a-1f3fc", "1f46e-1f3fd", "1f575-1f3fd", "1f482-1f3fd", "1f477-1f3fd", "1f9d9-1f3fd", "1f9da-1f3fd", "1f9db-1f3fd", "1f9dd-1f3fd", "1f9de-1f3fd", "1f9df-1f3fd", "1f48f-1f3fd", "1f491-1f3fd", "1f46a-1f3fd", "1f46e-1f3fe", "1f575-1f3fe", "1f482-1f3fe", "1f477-1f3fe", "1f9d9-1f3fe", "1f9da-1f3fe", "1f9db-1f3fe", "1f9dd-1f3fe", "1f9de-1f3fe", "1f9df-1f3fe", "1f48f-1f3fe", "1f491-1f3fe", "1f46a-1f3fe", "1f46e-1f3ff", "1f575-1f3ff", "1f482-1f3ff", "1f477-1f3ff", "1f9d9-1f3ff", "1f9da-1f3ff", "1f9db-1f3ff", "1f9dd-1f3ff", "1f9de-1f3ff", "1f9df-1f3ff", "1f48f-1f3ff", "1f491-1f3ff", "1f46a-1f3ff"]
    with open("static/emoji-standard.txt", "r") as f:
        for line in f:
            if line.startswith("# group"):
                current_group = line[9:-1].strip().replace(' ','-').replace('&','and').lower()
                d[current_group] = OrderedDict()
            if line[0] != "#" and line.strip():
                code = line.split(';')[0].strip().replace(' ','-').lower()
                f_q = "non-fully-qualified" not in line
                desc = line.split('#')[1][:-1]
                url = "https://twemoji.maxcdn.com/2/72x72/"+code+".png"
                if requests.get(url).status_code != 404 and \
                        (("person" not in desc and "people" not in desc and \
                        code not in exceptions) or \
                        ("man " in desc and f_q)):
                    response = requests.get(url, stream=True)
                    path = 'static/img/emojis/'+code+'.png'
                    with open(path, 'wb') as out_file:
                        shutil.copyfileobj(response.raw, out_file)
                    del response
                    if "skin tone" not in line:
                        x = 32 * (i['a'] % 52)
                        y = 32 * (i['a'] // 52)
                        add_image(path,'static/img/sheet_a.png',str(x),str(y))
                        i['a'] += 1
                        print(code + desc + " " + str(x) + " " + str(y))
                        d[current_group][code] = { 'x' : x, 'y' : y }
                    else:
                        tone = line.split(' ')[1].strip().lower()
                        toneless_code = ''.join(code.split('-'+tone))
                        if not toneless_code in d[current_group]:
                            toneless_code = '-fe0f'.join(code.split('-'+tone))
                        if not 'tones' in d[current_group][toneless_code]:
                            d[current_group][toneless_code]['tones'] = {}
                        x = 32 * (i[tone[-1]] % 14)
                        y = 32 * (i[tone[-1]] // 14)
                        add_image(path,'static/img/sheet_'+tone[-1]+'.png',str(x),str(y))
                        print('  ' + tone + desc + " " + str(x) + " " + str(y))
                        i[tone[-1]] += 1
                        d[current_group][toneless_code]['tones'][tone[-1]+' '+code] = { 'x' : x, 'y' : y }
    with open("static/emoji-lookup.json", "w") as outfile:
        outfile.write(json.dumps(d, indent=4))

def add_image(p,s,x,y):
    cmd = "mogrify -resize 32x "+p
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    cmd = "convert "+s+" "+p+" -geometry +"+x+"+"+y+" -composite "+s
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

if __name__ == '__main__':
    init_spritesheet('static/img/sheet_a.png', '1664','932')
    init_spritesheet('static/img/sheet_b.png','448','416')
    init_spritesheet('static/img/sheet_c.png','448','416')
    init_spritesheet('static/img/sheet_d.png','648','416')
    init_spritesheet('static/img/sheet_e.png','448','416')
    init_spritesheet('static/img/sheet_f.png','448','416')
    download_emojis()

