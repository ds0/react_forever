from database import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Boolean, DateTime, Column, Integer, \
                       String, ForeignKey
from models import user

class Link(Base):
    __tablename__ = 'link'
    id = Column(Integer, primary_key=True)
    url = Column(String(512))

class Reaction(Base):
    __tablename__ = 'reaction'
    id = Column(Integer, primary_key=True)
    user_id = Column('user_id', Integer(), ForeignKey('user.id'))
    user_object = relationship(user.User, backref='reactions')
    link_id = Column('link_id', Integer(), ForeignKey('link.id'))
    link_object = relationship(Link, backref='reactions')
    emoji = Column(String(100))

